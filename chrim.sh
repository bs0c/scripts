#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

load_lib()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        local SRC='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
        [[ -f $BASH_USER_LIB ]] || curl --silent $SRC --output "$BASH_USER_LIB"
    fi
    source "$BASH_USER_LIB"
}

usage()
{
    cat <<EOF

    Usage: $(basename "${BASH_SOURCE[0]}") [path] [-h] [-v]

    Improved chroot.

    Set chroot environment for current directory by default. Need root.
    It mounts the next directories: sys proc dev run tmp, and unmounts after exit.

    Available options:

    -h, --help      Print this help and exit
    -v, --verbose   Print script debug info

EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
    for catalog in sys proc dev run tmp; do
        if mount | grep --quiet "$path/$catalog"; then
            umount -R "$path/$catalog"
        fi
    done
    rm --force "$bashrc_file"
}

parse_params()
{
    path=''
    bashrc_file=$(mktemp)
    args=("$@")

    while :; do
        case "${1-}" in
            -h | --help) usage ;;
            -v | --verbose) set -x ;;
            -?*) die "Unknown option: $1" ;;
            *) path=${1-}; break;;
        esac
        shift
    done
    [[ -z $path ]] && path="$PWD"
    [[ -d $path ]] || die "Path: \"$path\" is not a directory"
    return 0
}

dependency_mount()
{
    mount --types proc /proc $path/proc
    mount --rbind /sys $path/sys
    mount --make-rslave $path/sys
    mount --rbind /dev $path/dev
    mount --make-rslave $path/dev
    mount --bind /run $path/run
    mount --make-slave $path/run 
    if [ -L /dev/shm ]; then
        rm /dev/shm
        mkdir /dev/shm
        mount --types tmpfs --options nosuid,nodev,noexec shm /dev/shm
        chmod 1777 /dev/shm /run/shm
    fi
    mount --rbind /tmp "$path"/tmp
}

set_chroot()
{
    cp /etc/resolv.conf "$path"/etc/
    cat <<-EOF >> "$bashrc_file"
		type env-update 2>/dev/null && env-update
		source /etc/profile
		export PS1="\[\033[1;34m\]┌─[\[\033[1;32m\]\u@\H\[\033[1;34m\]]─[\[\033[1;32m\]\w\[\033[1;34m\]]─[\[\033[1;32m\]chroot\[\033[1;34m\]]\[\033[0m\]\n\[\033[1;34m\]└─$\[\033[0m\] "
EOF
    chroot "$path" /bin/bash --init-file "$bashrc_file"
}

load_lib
check_root
parse_params "$@"
dependency_mount
set_chroot
