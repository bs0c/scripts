#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

load_lib()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        local SRC='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
        [[ -f $BASH_USER_LIB ]] || curl --silent $SRC --output "$BASH_USER_LIB"
    fi
    source "$BASH_USER_LIB"
}

usage()
{
      cat <<EOF
      Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [action] [--user|--system] "{user_scripts}"

      Little package script manager.

      Install or remove user scripts in "$HOME/.local/bin" by default, or "/usr/local/bin/" if you have used --system

      Available options:
      action:
      -i, --install   by default
      -r, --remove
      -l, --list      Display a list of installed scripts.
      --user          by default.
      --system        Copy or remove a script to /usr/bin.
      -h, --help      Print this help and exit.
      -v, --verbose   Print script debug info.
EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
}

parse_params()
{
    dest="$HOME/.local/bin"
    log_dir="$HOME/.config/lpsm"
    log_file="$log_dir/lpsm.log"
    mode=install
    while :; do
        case "${1-}" in
            -i | --install) mode=install ;;
            -r | --remove)  mode=remove ;;
            -l | --list)    mode=list ;;
            --user)   dest="$HOME/.local/bin" ;;
            --system) dest="/usr/local/bin";;
            -h | --help) usage ;;
            -v | --verbose)
                set -x
                PS4='`basename $0` [$LINENO]: '
                ;;
            --no-color) NO_COLOR=1 ;;
            -?*) die "Unknown option: $1" ;;
            *)
                [[ -n ${1-} ]] && scripts=$1
                break ;;
        esac
        shift
    done

    args=("$@")
    [[ -n ${scripts-} || $mode == list ]] || die "Missing required parameter: user_scripts"

    return 0
}

pkg_install()
{
    __mkdir $dest
    for script in $scripts; do
        src=$(find "$HOME/develop" -type f -name "$script")
        if [[ -f $src ]]; then
            cp "$src" "$dest/${script%.*}"
            ###   if it is an individual project   ###
            if [[ ${src%/*} =~ ${script%.*}$ ]]; then
                local ignore_list="LICENSE README.md CHANGELOG.md .git .gitignore $script"
                local home_conf="$HOME/.config/${script%.*}"
                [[ -d $home_conf ]] || mkdir --parents "$home_conf"
                for item_in_dir in "${src%/*}"/*; do
                    if [[ $ignore_list =~ ${item_in_dir##*/} ]]; then continue; fi
                    cp --recursive --update=none "$item_in_dir" "$home_conf"
                done
            fi
            ###   write in log   ###
            __mkdir "$log_dir"
            __touch "$log_file"
            if ! grep -q "$dest/${script%.*}" "$log_file"; then
                echo "$dest/${script%.*}" >> "$log_file"
            fi
            msg "\"$script\" ${GREEN}installed${NOFORMAT} in \"$dest\""
        else 
            msg "Can't install \"$script\", ${RED}file doesn't exist${NOFORMAT}"
        fi
    done
}

pkg_remove()
{
    for script in $scripts; do
        src=$(find "$dest" -type f -name "$script")
        if [[ -f $src ]]; then
            rm --force "$src"
            ###   remove from log   ###
            if [[ -f $log_file ]] ; then
                sed -i -e "s|$dest/$script||g" "$log_file"
            fi
            msg "\"$script\" ${GREEN}removed${NOFORMAT} from \"$dest\""
        else 
            msg "Can't remove \"$script\", ${RED}file doesn't exist${NOFORMAT}"
        fi
    done
}

pkg_list()
{
    if [[ -f $log_file ]]; then
        while read -r line; do
            [[ $line =~ ^[:space:]*$ ]] && continue
            msg "${GREEN}${line##*/}${NOFORMAT} in ${line%/*}"
        done < "$log_file"
    fi
}

load_lib
parse_params "$@"
setup_colors
pkg_$mode ${scripts-}
