#!/usr/bin/env bash


# We need a lot of tests!!!

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

load_lib()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        local SRC='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
        type curl &>/dev/null || {
            echo "To continue, you should install curl"
            exit 1
        }
        curl --silent $SRC --output "$BASH_USER_LIB"
    fi
    source "$BASH_USER_LIB"
}

usage()
{
      cat <<EOF
      Usage: $(basename "${BASH_SOURCE[0]}") [path_to_repo] [-f file_name] [-k private_key] [-h] [-v]

      Pull, commit, and push the target repository. If local changes are possible, script stashes
      them and returns after merge. Script fails with error if we have conflict during merge.

      Available options:

      -k, --key       Private key for remote repository without password.
      -f, --file      Get list of repositories from a file.
      -h, --help      Print this help and exit.
      -v, --verbose   Print script debug info.
EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
}

parse_params()
{
    key=
    repos=

    while :; do
        case "${1-}" in
            -k | --key)
                key=${2-}
                shift
                ;;
            -f | --file)
                repos=$(< ${2-})
                shift
                ;;
            -h | --help) usage ;;
            -v | --verbose)
                set -x
                PS4='`basename $0` [$LINENO]: '
                ;;
            --no-color) NO_COLOR=1 ;;
            -?*) die "Unknown option: $1" ;;
            *) [[ -n ${1-} ]] && repos+="$1 "
               [[ -z ${1-} ]] && break ;;
        esac
        shift
    done

    args=("$@")

    if [[ -z ${repos-} ]]; then          # try to get directory from local config
        [[ -n $XDG_CONFIG_HOME ]] || $XDG_CONFIG_HOME="$HOME/.config"
        repos=$(< "$XDG_CONFIG_HOME/git-routine/repos")
    fi
    [[ -z ${repos-} ]] && die "Missing required parameter: repository"

    return 0
}

is_pull()
{
    if [[ -n $(git fetch --porcelain) || -n $(git diff --stat HEAD...HEAD@{upstream}) ]]; then
        [[ -z $(git status --porcelain) ]] || git stash push --quiet
        if ! git merge --quiet; then
            local error="$repo repository has conflict before merge"
        fi
        [[ -z $(git stash list) ]] || {
            if ! git stash pop --quiet &>/dev/null; then
                local error="$repo repository has conflict after merge, before stash pop"
            fi
        }
        [[ -z ${error-} ]] || die "$error"
        msg "\"$repo\": repository has been ${GREEN}updated${NOFORMAT}"
    fi
}

is_commit()
{
    if [[ -n $(git status --porcelain) ]]; then
        git add .
        git commit --quiet --message="daily changes: $(date +%Y-%m-%d)" || die "Cannot commit in \"$repo\""
        msg "\"$repo\": changes have been ${GREEN}commited${NOFORMAT}"
    fi
}

is_push()
{
    if git status | grep -q "git push"; then
        # use a key without a password
        [[ -z ${key-} ]] || {
            if ! git config core.sshCommand | grep -q "$key"; then
                git config --local core.sshCommand "ssh -o IdentitiesOnly=yes -i $key -F /dev/null"
            fi
        }
        git push --quiet || die "Cannot push in \"$repo\""
        msg "\"$repo\": commits have been ${GREEN}pushed${NOFORMAT}"
    fi
}

git_routine()
{
    for repo in $repos; do
        if [[ $repo =~ ^[^\.\/] && $repo =~ '-' ]]; then    # check related path or systemd path
            repo="/${repo//-/\/}"                           # change etc-systemd => /etc/systemd
        fi
        [[ -d $repo/.git ]] || die "\"$repo\" doesn't have git repository"
        cd "$repo"
        is_pull
        is_commit
        is_push
    done
    msg "\"$repo\": all work ${GREEN}done${NOFORMAT}"
}

load_lib
parse_params "$@"
setup_colors
git_routine
