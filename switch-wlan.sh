#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)
script_name=$(basename "${BASH_SOURCE[0]}" .sh)

load_lib()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        local SRC='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
        type curl &>/dev/null || {
            echo "To continue, you should install curl"
            exit 1
        }
        curl --silent $SRC --output "$BASH_USER_LIB"
    fi
    source "$BASH_USER_LIB"
}

usage()
{
      cat <<EOF

      Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-w <wlan_nic>] [-e <ethernet_nic>] [-r, --remove]

      Script writes configuration files for <wlan_nic> to switch to hostpot mode if <ethernet_nic> is routable,
      and to client mode if <ethernet_nic> isn't. If you didn't set <wlan_nic> or <ethernet_nic>,
      the script takes a nic from the default route and sets it for <ethernet_nic>, and sets a first
      wlan-type nic for <wlan_nic>. Run the script and follow the instructions. You will need root.
      Based on wpa_supplicant, systemd, systemd-networkd, iproute2

      Available options:
      -e, --ethernet    Set ethernet nic name
      -w, --wlan        Set wlan nic name
      -n, --name        Set wifi name
      -p, --password    Set wifi password
      -N, --network     Set wifi network
      -r, --remove      Remove configuration files from the system, you must set <wlan_nic> to remove all config.
      -h, --help        Print this help and exit
      -v, --verbose     Print script debug info

EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
    [[ -z ${is_remove-} ]] || {
        msg "[-] remove config:"
        if systemctl --quiet is-active ${sys_timer##*/}; then
            systemctl --quiet disable --now ${sys_timer##*/} 
        fi
        if systemctl --quiet is-active ${sys_service##*/}; then
            systemctl --quiet disable --now ${sys_service##*/}
        fi
        rm --verbose --force \
            $sys_service $sys_timer $sys_wpa_client $sys_wpa_hostpot \
            $sys_net_client $sys_net_hostpot \
            $sys_script \
            $wpa_client $wpa_hostpot
        msg "${GREEN}[+]${NOFORMAT} remove config"
    }
}

parse_params()
{
    while :; do
        case "${1-}" in
            -e | --ethernet)
                eth="${2-}"
                shift
                ;;
            -w | --wlan)
                wlan="${2-}"
                shift
                ;;
            -n | --name)
                wifi_name="${2-}"
                shift
                ;;
            -p | --password)
                wifi_pass="${2-}"
                shift
                ;;
            -N | --network)
                wifi_addr="${2-}"
                shift
                ;;
            -r | --remove) is_remove=1 ;;
            -h | --help) usage;;
            -v | --verbose)
                set -x
                PS4='`basename $0` [$LINENO]: '
                ;;
            --no-color) NO_COLOR=1 ;;
            -?*) die "Unknown option: $1" ;;
            *)
                [[ -n ${1-} ]] && scripts=$1
                break ;;
        esac
        shift
    done

    args=("$@")

    setup_colors

    local conf="$script_dir/$script_name.conf"
    [[ -f $conf ]] && source "$conf"

    [[ -z $wlan || -z $eth || -z $wifi_name || -z $wifi_pass || -z $wifi_addr ]] && {
        die "Some variables in your config haven't set up"
    }

    wpa_client="/etc/wpa_supplicant/${wlan}-client.conf"
    wpa_hostpot="/etc/wpa_supplicant/${wlan}-hostpot.conf"
    sys_net_client="/etc/systemd/network/${wlan}-client.network"
    sys_net_hostpot="/etc/systemd/network/${wlan}-hostpot.network"
    sys_service="/etc/systemd/system/${script_name}.service"
    sys_timer="/etc/systemd/system/${script_name}.timer"
    sys_wpa_client="/etc/systemd/system/wpa_supplicant-client@.service"
    sys_wpa_hostpot="/etc/systemd/system/wpa_supplicant-hostpot@.service"
    sys_script="/usr/bin/${script_name}"

    [[ -z ${is_remove-} ]] || {
        cleanup
        exit
    }

    return 0
}

write_config()
{
    msg "[-] write config...\c"
    ###   write conf in /etc/wpa_supplicant   ###
    cat <<-EOF > "$wpa_client"
		ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=root
		update_config=1

		mac_addr=1
		preassoc_mac_addr=1
		gas_rand_mac_addr=1

		network={
		    ssid="$wifi_name"
		    key_mgmt=WPA-PSK
		    psk="$wifi_pass"
		}
EOF
    cat <<-EOF > "$wpa_hostpot"
		ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=root
		update_config=1

		mac_addr=1
		preassoc_mac_addr=1
		gas_rand_mac_addr=1

		network={
		    ssid="$wifi_name"
		    mode=2
		    key_mgmt=WPA-PSK
		    psk="$wifi_pass"
		    frequency=2412
		}
EOF
    ###   write conf in /etc/systemd/network   ###
    cat <<-EOF > "$sys_net_client"
		[Match]
		Name=$wlan

		[Network]
		DHCP=yes
EOF
    cat <<-EOF > "$sys_net_hostpot"
		[Match]
		Name=$wlan

		[Network]
		Address=$wifi_addr
		DHCPServer=true
		IPMasquerade=ipv4

		[DHCPServer]
		PoolOffset=100
		PoolSize=20
		EmitDNS=yes
		DNS=1.1.1.1
EOF
    ###   write conf in /etc/systemd/system   ###
    cat <<-EOF > "$sys_timer"
		[Unit]
		Description=Check ethernet connection: "$eth" is routable every 2 minutes

		[Timer]
		OnUnitInactiveSec=2m

		[Install]
		WantedBy=default.target
EOF
    # cat <<-EOF > "$sys_service"
		# [Unit]
		# Description=Switch "$wlan" to hostpot mode if "$eth" is routable and to client mode if doesn't
		# Documentation=man:systemd-networkd-wait-online.service(8)
		# DefaultDependencies=no
		# Conflicts=shutdown.target
		# Requires=systemd-networkd.service
		# After=systemd-networkd.service
		# Before=network-online.target shutdown.target
		# OnSuccess=wpa_supplicant-hostpot@$wlan.service
		# OnFailure=wpa_supplicant-client@$wlan.service

		# [Service]
		# Type=oneshot
		# ExecStart=/usr/lib/systemd/systemd-networkd-wait-online -i $eth:routable

		# [Install]
		# WantedBy=network-online.target
# EOF
    cat <<-EOF > "$sys_service"
		[Unit]
		Description=Switch "$wlan" to hostpot mode if "$eth" is routable and to client mode if doesn't
		Documentation=man:systemd-networkd-wait-online.service(8)
		DefaultDependencies=no
		Conflicts=shutdown.target
		Requires=systemd-networkd.service
		After=systemd-networkd.service
		Before=network-online.target shutdown.target

		[Service]
		Type=oneshot
		ExecStart=/usr/bin/$script_name

		[Install]
		WantedBy=network-online.target
EOF
    cat <<-EOF > "$sys_wpa_client"
		[Unit]
		Description=WiFi - Client mode
		Requires=sys-subsystem-net-devices-%i.device
		After=sys-subsystem-net-devices-%i.device
		Before=network.target
		BindsTo=${sys_timer##*/}
		Wants=network.target

		[Service]
		Type=simple
		WorkingDirectory=/etc/systemd/network
		Environment="CONF=%I-client.network"
		Environment="PREF=01-"
		ExecStartPre=/usr/bin/sleep 3
		ExecStartPre=/usr/bin/mv \${CONF} \${PREF}\${CONF}
		ExecStartPre=/usr/bin/systemctl daemon-reload
		ExecStartPre=/usr/bin/networkctl reload
		ExecStart=/usr/sbin/wpa_supplicant -c/etc/wpa_supplicant/%I-client.conf -i%I
		ExecReload=kill -HUP \$MAINPID
		ExecStop=kill -TERM \$MAINPID
		ExecStopPost=/usr/bin/mv \${PREF}\${CONF} \${CONF}

		[Install]
		WantedBy=multi-user.target
EOF
    cat <<-EOF > "$sys_wpa_hostpot"
		[Unit]
		Description=WiFi - Client mode
		Requires=sys-subsystem-net-devices-%i.device
		After=sys-subsystem-net-devices-%i.device
		Before=network.target
		BindsTo=${sys_timer##*/}
		Wants=network.target

		[Service]
		Type=simple
		WorkingDirectory=/etc/systemd/network
		Environment="CONF=%I-hostpot.network"
		Environment="PREF=01-"
		ExecStartPre=/usr/bin/sleep 3
		ExecStartPre=/usr/bin/mv \${CONF} \${PREF}\${CONF}
		ExecStartPre=/usr/bin/systemctl daemon-reload
		ExecStartPre=/usr/bin/networkctl reload
		ExecStart=/usr/sbin/wpa_supplicant -c /etc/wpa_supplicant/%I-hostpot.conf -i%I
		ExecReload=kill -HUP \$MAINPID
		ExecStop=kill -TERM \$MAINPID
		ExecStopPost=/usr/bin/mv \${PREF}\${CONF} \${CONF}

		[Install]
		WantedBy=multi-user.target
EOF
    ###   write sys_script in /usr/bin   ###
    cat <<-EOF > "$sys_script"
		#!/usr/bin/env bash

		[[ \$(id -u) -eq 0 ]] || exit 1

		wlan=$wlan
		wpa_client=wpa_supplicant-client@\${wlan}.service
		wpa_hostpot=wpa_supplicant-hostpot@\${wlan}.service
		opt="--quiet"

		if networkctl | grep \$opt --perl-regexp '${eth}(?=.*routable)' ;then
		    systemctl \$opt is-active \$wpa_hostpot || {
		        systemctl \$opt is-active \$wpa_client && {
		            systemctl \$opt stop \$wpa_client
		            sleep 5
		        }
		        systemctl \$opt start \$wpa_hostpot
		    }
		else
		    systemctl \$opt is-active \$wpa_client || {
		        systemctl \$opt is-active \$wpa_hostpot && {
		            systemctl \$opt stop \$wpa_hostpot
		            sleep 5
		        }
		        systemctl \$opt start \$wpa_client
		    }
		fi
EOF
    chmod +x "$sys_script"
    msg "${GREEN}\r[+]${NOFORMAT}"
}

run_service()
{
    msg "[-] run service...\c"
    systemctl daemon-reload
    systemctl enable --quiet --now $sys_service
    systemctl enable --quiet --now $sys_timer
    msg "${GREEN}\r[+]${NOFORMAT}"
}


load_lib
parse_params "$@"
check_app systemctl networkctl wpa_supplicant ip
check_root
write_config
run_service
