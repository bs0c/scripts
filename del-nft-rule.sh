#!/usr/bin/env bash

set -Eeuo pipefail

usage() {
    cat <<EOF

    Usage: $(basename "${BASH_SOURCE[0]}") "string"

    Delete rule with comment. Comment must be without spaces or enclosed in "".

EOF
    exit
}

main ()
{
    local table=""
    local chain=""
    local rules=()
    [[ $# -eq 1 ]] || usage
    IFS=$'\n'
    for line in $(nft --handle list ruleset); do
        if [[ $line =~ table ]]; then
            table=$(echo "$line" | sed -r 's/\s*table\s*(.*)\s*\{.*/\1/')
            chain=''
        fi
        if [[ $line =~ chain ]]; then
            chain=$(echo "$line" | sed -r 's/\s*chain\s*(.*)\s*\{.*/\1/')
        fi
        if [[ $line =~ ${1:-} && -n $table && -n $chain ]]; then
            num=$(echo "$line" | awk '{print $NF}')
            rules+=("$table $chain handle $num")
        fi
    done
    for line in ${rules[*]}; do
        nft delete rule $line
    done
}

main "$@"
