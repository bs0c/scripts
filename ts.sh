#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)


load_lib()
{
    [[ -n ${BASH_USER_LIB-} ]] || local BASH_USER_LIB="$script_dir/lib.sh"
    [[ -f $BASH_USER_LIB ]]    || {
        local src='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
        type curl &>/dev/null  || {
            echo "To continue, you should install curl"
            exit 1
        }
        curl --silent $src --output "$BASH_USER_LIB"
    }
    source "$BASH_USER_LIB"
}

usage()
{
    cat <<EOF
    Usage: $(basename "${BASH_SOURCE[0]}") {create|kill} {session_name} [-h] [-v] 

    Manager tmux session.

    This script can create several sessions for my needed.  You can run it under tmux,
    systemd and without tmux.  You defenetly don't need it =)

    Available options:

    -h, --help      Print this help and exit
    -v, --verbose   Print script debug info
EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
}

parse_params()
{
    start_w=4
    mode="create"
    backend_ssh="autossh"
    is_win=
    list_session=
    list_windows=
    while :; do
        case "${1-}" in
            create|kill) mode=$1 ;;
            -w | --windows) is_win=yes ;;
            -h | --help) usage ;;
            -v | --verbose)
                set -x
                PS4='`basename $0` [$LINENO]: '
                ;;
            --no-color) NO_COLOR=1 ;;
            -?*) die "Unknown option: $1" ;;
            *) if [[ -n ${1-} ]]; then
                   if [[ -n $is_win ]]; then
                       list_windows+=" $1"
                   else
                       list_session+=" $1"
                   fi
               else
                   break
               fi
               ;;
        esac
        shift
    done
    args=("$@")
    [[ -z "${mode-}" ]] && die "Missing required parameter: mode"
    [[ -z "${list_session-}" ]] && die "Missing required parameter: session name"
    return 0
}

kill_session()
{
    for name_s in $list_session; do
        if tmux has-session -t $name_s 2>/dev/null; then
            tmux kill-session -t $name_s
            msg "${GREEN}$name_s${NOFORMAT} session ${GREEN}removed${NOFORMAT}"
        else
            msg "${GREEN}$name_s${NOFORMAT} session ${RED}does not exist${NOFORMAT}"
        fi
    done
}

close_first_window()
{
    [[ $(tmux list-windows -F '#I' -t $name_s | head -n 1) != 1 ]] || {
        ( tmux ${cmd_begin-} \
            select-window -t:$start_w \; \
            kill-window -t:1 ${cmd_end-}
            ) 1>/dev/null
    }
}

get_next_free_index()
{
    local free_index=$start_w
    local stop=$(tmux list-windows -F '#I' -t dev | tail -n 1)
    while (($free_index <= $stop)); do
        ((free_index++))
    done
    echo $free_index
}

init_virt()
{
    num_w=$start_w
    opt_ssh="-M 0"
    for name_w in $list_windows; do
        if ! (tmux list-windows -F '#W' -t "$name_s" | grep -q "^${name_w}\$"); then
            ( tmux ${cmd_begin-} \
                new-window -n $name_w -t $num_w "$backend_ssh $opt_ssh ${name_w}-r" \; \
                split-window -h "$backend_ssh $opt_ssh ${name_w}-u" \; \
                select-pane -t 0 ${cmd_end-}
                ) 1>/dev/null
        fi
        ((num_w++))
    done
    [[ -z ${is_created-} ]] || close_first_window
}

init_dev()
{
    path="$HOME/develop"
    num_w=$(get_next_free_index)
    for name_w in ${list_windows-}; do
        if tmux list-windows -F '#W' -t "$name_s" | grep -q "^${name_w}\$"; then
            msg "\"$name_s\" session already has \"$name_w\" window ${RED}skip${NOFORMAT}..."
            continue
        fi
        [[ -d $path/$name_w ]] || {
            msg "Cannot find \"$name_w\" in \"$HOME/develop\", ${RED}skip${NOFORMAT}..."
            continue
        }
        ( tmux ${cmd_begin-} \
            new-window -n $name_w -t $num_w -c "$path/$name_w" 'ranger' \; \
            split-window -h -c "$path/$name_w" \; \
            split-window -v -l 15% -c "$path/$name_w" \; \
            select-pane -t 0 ${cmd_end-}
                ) 1>/dev/null
        msg "${GREEN}$name_w${NOFORMAT} in $name_s session ${GREEN}created${NOFORMAT}"
        ((num_w++))
    done
    [[ -z ${is_created-} ]] || close_first_window
}

init_adm()
{
    XDG_CONFIG_HOME="$HOME/.config"
    num_w=$start_w
    name_w="log"
    (
    tmux ${cmd_begin-} \
        new-window -n $name_w -t $num_w "dmesg -w" \; \
        split-window -h "journalctl -xef" \; \
        select-pane -t 0 ${cmd_end-}
            ) 1>/dev/null
    name_w="local"
    ((num_w++))
    (
    tmux ${cmd_begin-} \
        new-window -n $name_w -t $num_w -c $XDG_CONFIG_HOME 'ranger' \; \
        split-window -h -c $XDG_CONFIG_HOME \; \
        split-window -v -l 15% -c "/" \; \
        select-pane -t 0 ${cmd_end-}
            ) 1>/dev/null
    name_w="system"
    ((num_w++))
    (
    tmux ${cmd_begin-} \
        new-window -n $name_w -t $num_w -c '/etc' \; \
        split-window -h -c '/var/log' \; \
        select-pane -t 0 ${cmd_end-}
            ) 1>/dev/null
    [[ -z ${is_created-} ]] || close_first_window
}

create_session()
{
    for name_s in $list_session; do
        if tmux has-session -t $name_s 2>/dev/null; then
            msg "${GREEN}$name_s${NOFORMAT} session ${RED}already exists${NOFORMAT}"
        else
            tmux new-session -s $name_s -d
            is_created=1
            msg "${GREEN}$name_s${NOFORMAT} session ${GREEN}created${NOFORMAT}"
        fi
        [[ "virt dev adm" =~ $name_s ]] || continue
        if [[ ${TERM_PROGRAM-} == tmux ]]; then
            export TMUX=''
            cmd_begin="attach-session -d -t $name_s ;"
            cmd_end="; detach"
        fi
        init_${name_s}
    done
}


load_lib
parse_params "$@"
setup_colors
check_app tmux $backend_ssh

${mode}_session

#orig_session=$(tmux display-message -p '#S')
#orig_window=$(tmux display-message -p '#I')
#orig_pane=$(tmux display-message -p '#P')
