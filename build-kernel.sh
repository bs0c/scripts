#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

load_lib()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        local SRC='https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh'
        [[ -f $BASH_USER_LIB ]] || curl --silent $SRC --output "$BASH_USER_LIB"
    fi
    source "$BASH_USER_LIB"
}

usage()
{
    cat <<EOF

    Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-n] [-s {NUM}] [-c]

    Make and install kernel, rebuild nvidia-drivers if nessesary and clean old kernel.

    Available options:

    -h, --help      Print this help and exit
    -v, --verbose   Print script debug info
    -n, --new       Build the newest kernel
    -c, --change    Change kernel settings with nconfig
    -s, --save      Leave only newest $save_last kernels by default.
    --no-color

EOF
    exit
}

cleanup()
{
    trap - SIGINT SIGTERM ERR EXIT
    [[ -z ${is_mount-} ]] || umount --quiet /boot
    [[ -z ${mirror_boot_path-} ]] || { 
        umount --quiet "$mirror_boot_path"
        rm --force --recursive "$mirror_boot_path"
    }
}

parse_params()
{
    is_mount=
    is_change=
    threads=5
    save_last=2
    work_dir="/usr/src/linux"
    while :; do
        case "${1-}" in
            -h | --help) usage ;;
            -v | --verbose) set -x ;;
            --no-color) NO_COLOR=1 ;;
            -c | --change) is_change=1 ;;
            -s | --save)
                save_last="${2-}"
                shift ;;
            -?*) die "Unknown option: $1" ;;
            *) break ;;
        esac
        shift
    done

    args=("$@")

    return 0
}

mount_boot()
{
    mount | grep --quiet " /boot " || mount /boot && is_mount=1
}

set_ver_kernel()
{
    local list_kernel=($(find "/usr/src" -maxdepth 1 -type d -name "linux-*"))
    msg "Kernel list:"
    for i in "${!list_kernel[@]}"; do
        printf "  [%d]\t%s\n" $i ${list_kernel[$i]##*/}
    done
    printf "${GREEN}choose kernel: ${NOFORMAT}"
    read
    [[  $REPLY -lt 0 || $REPLY -ge ${#list_kernel[@]} ]] && die "wrong number"
    rm --force "$work_dir"
    ln --symbolic "${list_kernel[$REPLY]}" "$work_dir"
}

set_conf_kernel()
{
    local list_config=($(find /boot -maxdepth 1 -type f -name "config*"))
    local list_config+=($(find /etc/kernels -maxdepth 1 -type f))
    msg "Kernel config list:"
    for i in "${!list_config[@]}"; do
        printf "  [%d] %s\n" $i ${list_config[$i]}
    done
    printf "${GREEN}choose config: ${NOFORMAT}"
    read
    [[  $REPLY -lt 0 || $REPLY -ge ${#list_config[@]} ]] && die "wrong number"
    cp --force "${list_config[$REPLY]}" "$work_dir/.config"
    make -C "$work_dir" oldconfig

    [[ -z ${is_change-} ]] || make -C "$work_dir" nconfig
}

build_kernel()
{
    msg "${GREEN}- build kernel...${NOFORMAT}"
    make -C "$work_dir" -j$threads
    msg "${GREEN}- install modules...${NOFORMAT}"
    make -C "$work_dir" modules_install
    msg "${GREEN}- intall kernel...${NOFORMAT}"
    make -C "$work_dir" install
}

build_vcard_modules()
{
    msg "${GREEN}- check video card modules...${NOFORMAT}"
    local src="/lib/modules/$(readlink "$work_dir" | sed s'/^\/.*linux-//')-x86_64/video"
    [[ -n "$(ls -A "$src" 2>/dev/null)" ]] || {
        EMERGE_DEFAULT_OPTS="" emerge nvidia-drivers
    }
}

clean()
{
    msg "${GREEN}- remove old kernel version, leave only $save_last newest kernels...${NOFORMAT}"
    eclean-kernel -n $save_last
}

sync_boot()
{
    mirror_boot_path=$(mktemp -d)
    local mirror_boot_dev="/dev/nvme1n1p1"
    local opt="--recursive --delete --one-file-system"
    mount $mirror_boot_dev $mirror_boot_path
    rsync $opt /boot/ $mirror_boot_path
}

load_lib
parse_params "$@"
setup_colors
check_root
mount_boot
set_ver_kernel
set_conf_kernel
build_kernel
build_vcard_modules
clean
sync_boot
